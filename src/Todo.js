import React from 'react';
import axios from 'axios';
import './Todo.css';

class Todo extends React.Component {
  constructor() {
    super();

    this.state = {
      newTodoName: '',
      todoList: [],
      isModalOpen: false,
    };
  }

  addNewTodo = () => {
    const { newTodoName } = this.state;
    axios.post('http://195.181.210.249:3000/todo/', { title: newTodoName, extra: "0" }).then(() => {
      this.getAndRenderTodos();
      this.setState({ newTodoName: '' })
    })
  }

  updateInputValue = event => {
    this.setState({ newTodoName: event.target.value });
  }

  removeTodoAndRenderList = id => {
    axios.delete(`http://195.181.210.249:3000/todo/${id}`).then(() => {
      this.getAndRenderTodos();
    });
  }

  setAsDoneAndRenderList = (id, status) => {
    let extra = status;

    if (extra === "0") {
      extra = "1";
    } else {
      extra = "0";
    }

    axios.put(`http://195.181.210.249:3000/todo/${id}`, { extra }).then(() => {
      this.getAndRenderTodos();
    });
  }

  // openEditModal(x) {
  //   // otwarcie modala
  //   updateTodoAndRenderList(x)
  // }

  updateTodoAndRenderList = id => {
    axios.put('', {}).then(() => {
      this.getAndRenderTodos();
    })
  }

  getAndRenderTodos = () => {
    axios.get('http://195.181.210.249:3000/todo/').then(resp => {
      const todosFromServer = resp.data;
      this.setState({
        todoList: todosFromServer
      })
    });
  }

  componentDidMount() {
    this.getAndRenderTodos();
  }

  render() {
    return (
      <main>
        <header>
          <label htmlFor="newTodoInput">Create TODO:</label>
          <input onChange={this.updateInputValue} value={this.state.newTodoName} type="text"/>
          <button onClick={this.addNewTodo}>ADD</button>
        </header>

        <section>
          <ul>
            {this.state.todoList.map((todo => <li key={todo.id}>
              <span className={ todo.extra === "1" ? 'todo-done' : '' }>{todo.title}</span>
              <button onClick={() => { this.removeTodoAndRenderList(todo.id) }}>DELETE</button>
              <button onClick={() => { this.setAsDoneAndRenderList(todo.id, todo.extra) }}>{ todo.extra === "1" ? 'REVERT' : 'DONE' }</button>
            </li>))}
          </ul>
        </section>
      </main>
    );
  }
}

export default Todo;
